package ch.epfl.data.bigdata.hw2;

import java.io.IOException;
import java.io.*;
import java.util.*;
	
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;
 	
public class Run {
	
	public static class Map1 extends MapReduceBase implements Mapper<LongWritable, Text, IntWritable, Text> {
		private static IntWritable newKey = new IntWritable();
		private static Text newValue = new Text();
	
		public void map(LongWritable key, Text value, OutputCollector<IntWritable,Text> output, Reporter reporter) throws IOException {
			String line = value.toString();
			
			String[] result = line.split(",");
			
			String lineId = result[0];	
			int lineIdInt = Integer.parseInt(lineId);
     			for (int i=1; i<result.length; i++) {
				int resultI = Integer.parseInt(result[i]);
				for (int j=1; j<result.length; j++) {
					int resultJ = Integer.parseInt(result[j]);
					if (resultI > resultJ && lineIdInt >  resultJ) {
						if (resultI > Integer.parseInt(lineId)) {
							newValue.set("(" + lineId + "," + result[i] + ")");
						} else {
							newValue.set("(" + result[i] + "," + lineId + ")");
						}
						newKey.set(resultJ);	
						output.collect(newKey,newValue);
					}
					//System.out.println(result[i]);		
					//while (tokenizer.hasMoreTokens()) {
					//word.set(result[i]);
					//output.collect(word, one);
				}
			}
		}
	}
 	
	public static class Reduce1 extends MapReduceBase implements Reducer<IntWritable, Text, IntWritable, Text> {
		public void reduce(IntWritable key, Iterator<Text> values, OutputCollector<IntWritable, Text> output, Reporter reporter) throws IOException {
			String sum = "";
			while (values.hasNext()) {
				sum += values.next().toString();
				
			}
			output.collect(key, new Text(sum));
		}
	}
 	
	public static class Map2 extends MapReduceBase implements Mapper<LongWritable, Text, Text, IntWritable> {
		private final static IntWritable one = new IntWritable(1);
		private Text word = new Text();
	
		public void map(LongWritable key, Text value, OutputCollector<Text, IntWritable> output, Reporter reporter) throws IOException {
			String line = value.toString();
			
			String[] result = line.split(",");
     			for (int x=0; x<result.length; x++) {
         			System.out.println(result[x]);		
			//while (tokenizer.hasMoreTokens()) {
				word.set(result[x]);
				output.collect(word, one);
			}
		}
	}
 	
	public static class Reduce2 extends MapReduceBase implements Reducer<Text, IntWritable, Text, IntWritable> {
		public void reduce(Text key, Iterator<IntWritable> values, OutputCollector<Text, IntWritable> output, Reporter reporter) throws IOException {
			int sum = 0;
			while (values.hasNext()) {
				sum += values.next().get();
			}
			output.collect(key, new IntWritable(sum));
		}
	}
 
	public static void main(String[] args) throws Exception {
		JobConf conf1 = new JobConf(Run.class);
		JobConf conf2 = new JobConf(Run.class);
		
		conf1.setJobName("hw2-phase1");
	 	conf2.setJobName("hw2-phase2");

		conf1.setOutputKeyClass(IntWritable.class);
		conf1.setOutputValueClass(Text.class);
	 	
		conf2.setOutputKeyClass(Text.class);
		conf2.setOutputValueClass(IntWritable.class);
	 	
		conf1.setMapperClass(Map1.class);
		conf1.setCombinerClass(Reduce1.class);
		conf1.setReducerClass(Reduce1.class);
 	
		conf1.setInputFormat(TextInputFormat.class);
		conf1.setOutputFormat(TextOutputFormat.class);
 
		FileSystem fs = FileSystem.get(conf1);
		fs.delete(new Path("std007/hw2/temp/"),true);	
		FileInputFormat.setInputPaths(conf1, new Path(args[0]));
		//FileOutputFormat.setOutputPath(conf1, new Path("std007/hw2/temp/"));
 		FileOutputFormat.setOutputPath(conf1, new Path(args[1]));
 	

	
		conf2.setMapperClass(Map2.class);
		conf2.setCombinerClass(Reduce2.class);
		conf2.setReducerClass(Reduce2.class);
 	
		conf2.setInputFormat(TextInputFormat.class);
		conf2.setOutputFormat(TextOutputFormat.class);
 	
		FileInputFormat.setInputPaths(conf2, new Path("std007/hw2/temp/"));
		FileOutputFormat.setOutputPath(conf2, new Path(args[1]));

		//conf1.set("mapred.max.split.size", "1020"); 	
		JobClient.runJob(conf1);
		//JobClient.runJob(conf2);
	}
}	
