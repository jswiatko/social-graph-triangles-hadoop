#!/bin/bash

hadoop fs -rmr /std007/hw2/output/

javac -cp /home/hduser/hadoop/hadoop-core-1.2.1.jar -d classes Run.java 
jar cvf hw2.jar -C classes/ .

hadoop jar hw2.jar ch.epfl.data.bigdata.hw2.Run /shared/bigdata/hw2/small/ /std007/hw2/output/ 

rm -r output/*
hadoop dfs -get /std007/hw2/output/* output
cat output/part-00000 


